# FCC spectrum license lookup and measurement

#### FCC spectrum database search

1. Find the frequency ranges used by LTE FDD in band 30. E.g., from here:
   
   [https://en.wikipedia.org/wiki/LTE_frequency_bands](https://en.wikipedia.org/wiki/LTE_frequency_bands)


2. Point your browser at the FCC spectrum database search page:
	
	[https://wireless2.fcc.gov/UlsApp/UlsSearch/searchLicense.jsp](https://wireless2.fcc.gov/UlsApp/UlsSearch/searchLicense.jsp)
	
3. Select the `Geographic` search option.

4. Enter the following search criteria:

	(a) State: Utah

	(b) County: UT - Salt Lake

	(c) Enter a frequency range that covers both the uplink and downlink for band 30.

5. Explore the results:
	
	(a) What radio service is associated with the licenses?
	
	(b) What channels and frequency ranges are covered by each license?
	
	(c) What frequency ranges in both upstream and downstream are collectively covered by these licenses?

#### Spectrum measurement using uhd_fft

For this session we will be POWDER fixed-endpoint nuc1 nodes:

| Room Number  | Fixed-endpoint node |
| --- | --- |
| 1 | b210-cpg-nuc1  |
| 2 |  b210-guesthouse-nuc1 |
| 3 |  b210-law73-nuc1 |
| 4 | b210-sagepoint-nuc1 |


1. *Representative from each group* log into the compute node associated with your SDR experiment. Run `uhd_fft` to try to determine whether these licenses are in use.

	(a) Select a center frequency "close" to the range you want to observe.

	(b) For the SDRs in the fixed-endpoints use a sample rate of 50 MS/s, i.e., `-s 50M`.

	(c) Execute `uhd_fft`: uhd_fft -s SAMPLE_RATE -f CENTER_FREQUENCY. E.g, if the center frequency of interest is 2160 MHz:
	```
	uhd_fft -s 50M -f 2160M
	```
	Note: You might have to run uhd_fft a number of times with different parameters to properly cover both the upstream and downstream frequency ranges. (Or, adjust it from within the GUI.)
	
	
	
2. Explore the results:

	(a) Is there activity on the downstream frequency range?

	(b) What about the upstream?

	(c) Any other noteworthy activity?
	 

#### More spectrum license exploration

1. Perform similar activities (i.e., FCC database search followed by `uhd_fft` measurements) for a `Market Based` license search with: 
		
	* Market Type: BEA
	* Market: BEA152 - Salt Lake City-Ogden, UT-ID
	* Radio service: AD - AWS-4 (2000-2020 MHz and 2180-2200 MHz)
			

2. Do a `Geographic` search with:
	
	* State: Utah
	* County: UT - Salt Lake
	* Frequency range: 2160 - 2180
		

3. Possible explanation for difference between AD (AWS-4) and AT (AWS-3) observations:
	
[https://www.fcc.gov/auction/97](https://www.fcc.gov/auction/97)

[https://www.fcc.gov/document/aws-2000-20202180-2200-mhz-aws-4-order-adopted](https://www.fcc.gov/document/aws-2000-20202180-2200-mhz-aws-4-order-adopted)

[https://www.forbes.com/sites/fredcampbell/2018/07/20/dish-network-terrestrial-spectrum-licenses-at-risk/](https://www.forbes.com/sites/fredcampbell/2018/07/20/dish-network-terrestrial-spectrum-licenses-at-risk/)	

